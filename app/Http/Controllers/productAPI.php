<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use App\Models\Product;
use Mail;

class productAPI extends Controller
{	
    public function uploadCSV(Request $request)
    {
 		if($request->method() == 'POST') {
        $validator = Validator::make($request->all(), 
              [ 
              'file' => 'required|mimes:csv,txt|max:2048',
             ]);   
 
    	if ($validator->fails()) {          
            return response()->json(['error'=>$validator->errors()], 401);                        
        }
        if ($file = $request->file('file')) {
            //store file into document folder
            $extension = $file->getClientOriginalExtension();
 			$path = $file->storeAs('public/documents','product'.'.'.$extension);
            //store your file into database
            $importCSV = $this->importCSVData();
            if($importCSV == true){
	            return response()->json([
	                "success" => true,
	                "message" => "CSV file successfully uploaded and saved to database",
	                "file" => $file
	            ]);
            }
        }
 		} else {
 			return response()->json([
	                "status" => 405,
	                "message" => "Method not allowed"
	            ]);
 		}
    }

    public function importCSVData() {
        //get the file
	    $path = base_path('public/documents/product.csv');
	    // Check for errors
	    $validateCSV = $this->validateCsv($path);
	    if($validateCSV == true) {
	    //turn into array
	    $file = file($path);
	    //remove first line
	    $data = array_slice($file, 1);
        //loop through file and split every 500 lines
	    $parts = (array_chunk($data, 500));
	    $i = 1;
	    foreach($parts as $line) {
	        $filename = base_path('resources/uploadcsv/'.date('y-m-d-H-i-s').$i.'.csv');
	        file_put_contents($filename, $line);
	        $i++;
	    }
	    //set the path for the csv files
	    $path = base_path("resources/uploadcsv/*.csv"); 
	    //run 2 loops at a time 
	    foreach (array_slice(glob($path),0,2) as $file) { 
	    	set_time_limit(50);
	        //read the data into an array
	        $data = array_map('str_getcsv', file($file));
	        //loop over the data
	        foreach($data as $row) {
	            //insert the record
	            $inserted_data=array('module_code'=>$row[1],
                                 'module_name'=>$row[2],
                                 'module_term'=>$row[3]
                            );
	            Product::create($inserted_data);
	        }
	        //delete the file
	        unlink($file);
    	}
    		return true;
    	}
    }

    public function validateCsv($pathToCsvFile)
    { 
        if(!file_exists($pathToCsvFile) || !is_readable($pathToCsvFile)){
            throw new \Exception('Filename doesn`t exist or is not readable.');
        }
        if (!$handle = fopen($pathToCsvFile, "r")) {
            throw new \Exception("Stream error");
        }
        $rowLength = array();
        $rowNumber = 0;
        while (($data = fgetcsv($handle)) !== FALSE) { 
        	$numcols = count($data);
            $rowLength[] = count($data);
            $rowNumber++;
        }
        // check column count
        if($numcols != 4) {
        	throw new \Exception("Error, column count is not 4");
        }
        // check row count
        if($rowNumber-1 != 1000) {
        	throw new \Exception("Error, row count is not 1000");
        } 
        fclose($handle);

        $rowKeyWithError   = array_search(max($rowLength), $rowLength); 
        $differentRowCount = count(array_unique($rowLength));

        // if there's a row that has more or less data, throw an error with the line that triggered it
        if ($differentRowCount !== 1) { 
            throw new \Exception("Error, data count from row {$rowKeyWithError} does not match header size");
        }
        return true;
    }


}
