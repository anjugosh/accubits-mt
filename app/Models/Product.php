<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
class Product extends Model
{
    public $fillable = ['module_code', 'module_name', 'module_term'];
}